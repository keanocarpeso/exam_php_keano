<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Employee_model extends CI_Model
{

	private $tableName = 'employees';

	public function employee_tree() {
        $this->db->select('
        					employeeNumber,
        				   	CONCAT(employees.firstName," ", employees.lastName) AS name,
        				   	jobTitle,
        				   	reportsTo
        						');
        $this->db->from($this->tableName);
        $query = $this->db->get();
        $result = $query->result_array();
        return $this->prep_employee($result);
	}

	public function prep_employee($result,$employee = null) {
		$data = array();
		foreach($result as $row){
			if($row['reportsTo'] == $employee['employeeNumber']){
				$check = $this->prep_employee($result,$row);
				if($check){
					$row['employeeUnder'] = $check;
				}
				$data[] = $row;
			}
		}
		return $data;
		// common_library::dump($data,1);
		/*$new_data = array();
		foreach($data as $row){
			$check = $this->get_employee_under($data,$row);
			if($check){
				$row['employeeUnder'] = $check;
			}
			$new_data[] = $row;
		}
		$more = array();
		foreach($new_data as $row){
			$check = $this->get_employee_under($new_data,$row);
			if($check){
				$row['employeeUnder'] = $check;
			}
			$more[] = $row;
		}
		$more1 = array();
		foreach($more as $row){
			$check = $this->get_employee_under($more,$row);
			if($check){
				$row['employeeUnder'] = $check;
			}
			$more1[] = $row;
		}
		common_library::dump($more1);*/
	}
	public function get_employee_under($employees,$employee){
		$data = array();
		foreach($employees as $key => $emp){
			if($emp['reportsTo'] == $employee['employeeNumber']){
				$data[] = $emp;
			}
		}
		return $data;
	}
	public function office_employees() {
		$this->db->select('offices.officeCode, offices.city');
        $this->db->select('(SELECT GROUP_CONCAT(CONCAT(employees.firstName," ",employees.lastName)) from employees where employees.officeCode = offices.officeCode) as name',FALSE);
        $this->db->select('(SELECT GROUP_CONCAT(employees.jobTitle) from employees where employees.officeCode = offices.officeCode) as jobTitle',FALSE);
        $this->db->select('(SELECT GROUP_CONCAT(employees.employeeNumber) from employees where employees.officeCode = offices.officeCode) as employeeNumber',FALSE);
        $this->db->from('offices');
        $query = $this->db->get();
        $offices = $query->result_array();
        // echo $this->db->last_query();
        $data = array();
        foreach($offices as $office){
        	$array_name = explode(",",$office['name']);
        	$array_jobTitle = explode(",",$office['jobTitle']);
        	$array_employeeNumber = explode(",",$office['employeeNumber']);
        	$count_employee = count($array_employeeNumber);
        	$office_data = array();
        	$office_employee = array();
			for($i=0; $i < $count_employee; $i++){
				$office_employee[$i][] = $array_name[$i];
				$office_employee[$i][] = $array_jobTitle[$i];
				$office_employee[$i][] = $array_employeeNumber[$i];
			}        	
			$office_data['officeCode'] = $office['officeCode'];
			$office_data['city'] = $office['city'];
			$office_data['employees'] = $office_employee;
			$data[] = $office_data;
        }
        return $data;
	}

}